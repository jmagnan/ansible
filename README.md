Ansible
=======

> **Develop for RHEL-like**

## Playbooks
- [X] K8s Cluster
  - [X] Master
  - [X] Node
- [X] Docker
   - [X] Rootless
   - [X] Traefik
   - [X] Portainer
- [ ] K3s Cluster
  - [ ] Master
  - [ ] Node

## Modules

- [X] Vault (ansible-vault python module)

## Callback

- [X] PDF Report (pdf_report)

## Required collection

- general.modprobe
- kubernetes.core

## Gitlab-CI

Test:
  - ansible-lint

## Script for install ansible user on linux target
```bash
#!/bin/bash

ANSIBLE_USER="ansible"
SSH_KEY=""

useradd $ANSIBLE_USER --create-home
passwd $ANSIBLE_USER

# SSHKEY
mkdir -p /home/$ANSIBLE_USER/.ssh
echo $SSH_KEY >> /home/$ANSIBLE_USER/.ssh/authorized_keys
chown -R $ANSIBLE_USER /home/$ANSIBLE_USER/.ssh 

# SUDO
mkdir -p /etc/sudoers.d/
echo "$ANSIBLE_USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$ANSIBLE_USER

cat << EOF >> /etc/ssh/sshd_config
Match User $ANSIBLE_USER
    PasswordAuthentication no
EOF

systemctl restart sshd
```