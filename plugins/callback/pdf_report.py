##
#
# Auteur: Jérémy Magnan
# Contact: jeremy@magnan.one
# Description: Callback qui génère des rapports en PDF
#
##

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.plugins.callback import CallbackBase
from ansible import constants as C

from weasyprint import HTML
from jinja2 import Template
from datetime import datetime
import os, time

class CallbackModule(CallbackBase):

    DOCUMENTATION = '''
        callback: pdf_report
        type: file
        short_description: 
        1. Write a report in PDF file in folder .report
        description:
            - Donne les stats générale du playbook
            - Liste les tâches avec leurs actions
            - En cas d'erreur le rapport indique l'erreur avec plug de debug
        requirements:
            - python os,datetime,weasyprint,jinja2
    '''

    CALLBACK_VERSION = 2.0      
    CALLBACK_TYPE = 'report' 
    CALLBACK_NAME = 'pdf_report'
    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self, *args, **kwargs):
        super(CallbackModule, self).__init__()

        self.start_time = time.time()
        datenow = datetime.now()

        self.DATE_FORMAT = datenow.strftime("%Y%m%d-%H%M%S")
        self.REPORT_PATH = f"{os.getcwd()}/.reports/"
        self.TEMPLATE_FILE = f"{os.getcwd()}/report.jina2"
        self.JINJA_VARS = dict(
            subtitle = "PDF report for Ansible",
            version = "2.0",
            start_time = time.strftime("%H:%M:%S", time.gmtime(self.start_time)),
            tasks = [],
            gather_facts = dict()
        )
        
    # Début du playbook
    def v2_playbook_on_start(self, playbook):
        self.JINJA_VARS['playbook'] = playbook
        self.JINJA_VARS['title'] = playbook.get_plays()[0]

    # Début d'une tâche
    def v2_playbook_on_task_start(self, task, is_conditional):
        self.JINJA_VARS['tasks'].append(task)

    # Tâche OK
    def v2_runner_on_ok(self, result):
        if 'ansible_facts' in result._result.keys():
            self.JINJA_VARS['gather_facts'][result._host] = result._result['ansible_facts']

    # Stats du playbook
    def v2_playbook_on_stats(self, stats):
        end_time = time.time()
        runtime = end_time - self.start_time
        self.JINJA_VARS['time'] = time.strftime("%H:%M:%S", time.gmtime(runtime))
        self.JINJA_VARS['end_time'] = time.strftime("%H:%M:%S", time.gmtime(end_time))
        self.JINJA_VARS['hosts'] = self.status_by_host(stats)
        self.gen_pdf()
        self._display.display("Report generate with success !", color=C.COLOR_OK)

    # Récupère le status pour chaque hôte
    def status_by_host(self, stats):
        hosts = sorted(stats.processed.keys())
        status = dict()
        for host in hosts:
            status[host] = dict(
                processed = stats.processed[host] if host in stats.processed.keys() is not None else 0,
                failures = stats.failures[host] if host in stats.failures.keys() is not None else 0,
                ok = stats.ok[host] if host in stats.ok.keys() is not None else 0,
                dark = stats.dark[host] if host in stats.dark.keys() is not None else 0,
                changed = stats.changed[host] if host in stats.changed.keys() is not None else 0,
                skipped = stats.skipped[host] if host in stats.skipped.keys() is not None else 0,
                rescued = stats.rescued[host] if host in stats.rescued.keys() is not None else 0,
                ignored = stats.ignored[host] if host in stats.ignored.keys() is not None else 0,
                custom = stats.custom[host] if host in stats.custom.keys() is not None else 0
            )
        return status

    # Génération du PDF
    def gen_pdf(self):
        if not os.path.exists(self.REPORT_PATH):
            os.makedirs(self.REPORT_PATH)
        HTML(string=self.jinja_template(), base_url=self.REPORT_PATH).write_pdf(f"{self.REPORT_PATH}{self.JINJA_VARS['title']} - {self.DATE_FORMAT}.pdf")

    def jinja_template(self):
        if os.path.exists(self.TEMPLATE_FILE):
            with open(self.TEMPLATE_FILE, 'r') as j2_template:
                return Template(j2_template.read()).render(self.JINJA_VARS)
        else:
            return Template("""
<html>
<head>
	<meta charset="utf-8">
	<title>{{ title }}</title>
	<meta name="description" content="{{ title }}">
	<style>
	@page {
	counter-increment: page;
		@bottom-right {
			background-color: #4869EE;
            color: #D4DCFF;
			content: counter(page);
			height: 1cm;
			text-align: center;
			width: 1cm;
		}
	}
	@page :first {
		background-color: #1B1E3D;
		background-size: cover;
		margin: 0;
	}
    @page :blank {
        @bottom-right { background: none; content: '' }
        @top-center { content: none }
        @top-right { content: none }
    }

	html {
		color: black;
		margin: 0;
	}

    h1{
        color: #4869EE;
    }

	#cover {
        color: #D4DCFF;
	}
    #cover div{
        font-size: 13pt;
        bottom: 2%;
        position: absolute;
        left: 30%;
    }
	#cover h1 {
		font-size: 38pt;
		margin: 5cm 2cm 0 2cm;
		width: 100%;
	}

	#cover h2{
		margin: 0 3cm 0 3cm;
		width: 100%;
	}

    .table_list{
        padding-bottom: 5%;
    }

    table.table_status{
        margin-bottom: 5%;
    }

    div.title_args{
        color: #a9a;
        font-size: 12pt;
        font-weight: bold;
        padding-right: .30cm;
        text-transform: uppercase;
        text-align: left;
    }

    .table_list th {
        color: #a9a;
        font-size: 12pt;
        font-weight: 400;
        padding-right: .30cm;
        text-transform: uppercase;
        text-align: left;
    }

    .table_list caption.table_title {
        font-weight:bold;
        font-size: 16pt;
        padding-bottom: 2%;
    }

    table.table_status  {
        border-collapse: collapse;
        width: 100%;
        padding-bottom: 5%;
    }
    .table_status caption.table_title {
        font-size: 21pt;
        text-align: center;
        padding-bottom: 2%;
    }
    .table_status th {
        border-bottom: .2mm solid #a9a;
        color: #a9a;
        font-size: 10pt;
        font-weight: 400;
        padding-bottom: .25cm;
        text-transform: uppercase;
    }
    .table_status td {
        padding-top: 2mm;
    }
    .table_status th, .table_status td {
        text-align: center;
    }

    tr.args{
        margin-left: 5%;
        color: rebeccapurple;
    }

	article {
		break-before: always;
	}

    .ok{
        color: #617F65;
    }

    .changed {
        color: #A75830;
    }

    .failed:not([text="0"]) {
        color: red;
    }
	</style>
</head>

<body>
	<article id="cover">
		<h1>{{ title }}</h1>
		<h2>{{ subtitle }}</h2>
        <div>Made with WeasyPrint  -  Version {{ version }}</div>
	</article>
	
    <article id="playbook">
        <h1>Playbook</h1>
        <table class="table_list">
        <caption class="table_title"></caption>
        <tr>
            <th>Playbook name</th>
            <td>{{ playbook.get_plays()[0] }}</td>
        </tr>
        <tr>
            <th>Start time</th>
            <td>{{ start_time }}</td>
        </tr>
        <tr>
            <th>End time</th>
            <td>{{ end_time }}</td>
        </tr>
        <tr>
            <th>Time of execution</th>
            <td>{{ time }}</td>
        </tr>
        <tr>
            <th>Path</th>
            <td>{{ playbook._basedir }}/{{ playbook._file_name }}</td>
        </tr>
        </table>
        {% for host in hosts %}
        <table class="table_status">
        <caption class="table_title">{{ host }}</caption>
        <thead>
            <tr>
            <th>Ok</th>
            <th>Changed</th>
            <th>Unreachable</th>
            <th>Failed</th>
            <th>Skipped</th>
            <th>Rescued</th>
            <th>Ignored</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td class="ok">{{ hosts[host]['ok'] }}</td>
            <td class="changed">{{ hosts[host]['changed'] }}</td>
            <td class="unreachable">{{ hosts[host]['dark'] }}</td>
            <td class="failed">{{ hosts[host]['failures'] }}</td>
            <td class="skipped">{{ hosts[host]['skipped'] }}</td>
            <td class="rescued">{{ hosts[host]['rescued'] }}</td>
            <td class="ignored">{{ hosts[host]['ignored'] }}</td>
            </tr>
        </tbody>
        </table>
        {% endfor %}
	</article>

    {% if gather_facts %}
	<article id="hosts">
    <h1>Hosts</h1>
    {% for host in gather_facts %}
    <table class="table_list">
        <caption class="table_title">{{ host }}</caption>
        <tr>
            <th>Server name</th>
            <td>{{ gather_facts[host].ansible_hostname }}</td>
        </tr>
        <tr>
            <th>OS</th>
            <td>{{ gather_facts[host].ansible_system }} {{ gather_facts[host].ansible_architecture }}</td>
        </tr>
        <tr>
            <th>Kernel</th>
            <td>{{ gather_facts[host].ansible_kernel }}</td>
        </tr>
        {% if gather_facts[host].ansible_distribution %}
        <tr>
            <th>Distribution</th>
            <td>{{ gather_facts[host].ansible_distribution }} {{ gather_facts[host].ansible_distribution_version }}</td>
        </tr>
        {% endif %}
        <tr>
            <th>Python version</th>
            <td>{{ gather_facts[host].ansible_python_version }}</td>
        </tr>
    </table>
    {% if gather_facts[host].ansible_default_ipv4 %}
    <div class="title_args">Network</div>
    <table class="table_list">
        <tr>
            <th>Interface</th>
            <td>{{ gather_facts[host].ansible_default_ipv4.interface }}</td>
        </tr>
        <tr>
            <th>IP</th>
            <td>{{ gather_facts[host].ansible_default_ipv4.address }}</td>
        </tr>
        <tr>
            <th>Gateway</th>
            <td>{{ gather_facts[host].ansible_default_ipv4.gateway }}</td>
        </tr>
        <tr>
            <th>Network</th>
            <td>{{ gather_facts[host].ansible_default_ipv4.network }}/{{ gather_facts[host].ansible_default_ipv4.prefix }}</td>
        </tr>
    </table>
    {% endif %}
    {% endfor %}
    </article>
    {% endif %}

	<article id="tasks">
        <h1>Tasks</h1>
        {%- for task in tasks %}
        <table class="table_list">
        <caption class="table_title">{{ task.get_name() }}</caption>
        <tr>
            <th>Line on playbook</th>
            <td>{{ task.get_path() }}</td>
        </tr>
        <tr>
            <th>Action</th>
            <td>{{ task._action }}</td>
        </tr>
        </table>
        {% if task._args %}
        <div class="title_args">Arguments</div>
        <table class="table_list">
        {% for arg in task._args %}
        <tr class="args">
            <th>{{ arg }}</th>
            <td>{{ task._args[arg] }}</td>
        </tr>
        {% endfor %}
        </table>
        {% endif %}
        {%- endfor %}
	</article>
    {{ gather_facts["localhost"] }}
</body>
</html>
""").render(self.JINJA_VARS)